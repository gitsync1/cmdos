# CmdOS

this is a basic, python, embeddable OS.

## Installation

Run these commands in the following order:

1st:
```
git clone -b https://git.bitsyncdev.com/bit-sync/CmdOS.git
```
2ed:
```
cd cmdos
```
3ed:
```
python3 main.py
```
Done!!
## Commands

### "about"

this prints info about your current installation

### "version"

prints current verison

### "update"

updates CmdOS

### "poss"

this command runs the Poss python package manager.

### "poss install {package}"

this poss command installs whatever package you put after poss install.

### "poss run {package}"

this poss command might not work for all packages, but most. It runs the package of your choosing.
### "poss version"

prints the version of poss

### "poss list"



this prints the current Poss version

## packages and software

CmdOS does not come with any software besides the package manager, Poss. 

to view all packages, go to [the Poss gitlab](https://gitlab.com/poss4/poss-package-manager/-/blob/main/PACKAGEINDEX.md?ref_type=heads&plain=0&blame=1) and read "PACKAGEINDEX.md".

